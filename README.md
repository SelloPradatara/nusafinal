<<<<<<< HEAD
## How to run Development

Install it and run:

```bash
yarn
yarn dev
```

## How to run Production

Install it and run:

```bash
yarn
yarn build
yarn start
```

## Folder Structure

- src
  - components: This directory contains reusable components that are used across multiple pages.
  - data: Here, you'll find the code responsible for fetching data from the backend using React Query. This directory stores the logic and functions for data retrieval.
  - helpers: The "helpers" directory consists of JavaScript utility functions, including any custom utilities built on top of libraries like lodash.
  - layouts: In this directory, you'll find page layouts and container customizations. It contains components or templates that define the overall structure of a page.
  - pages: The "pages" directory holds the Next.js pages. These files represent the different routes or views of your application. Each page file corresponds to a specific URL.
  - services: This directory contains code related to API integrations, including fetching data, making requests using libraries like Axios or XHR, and handling API-related functionality.
  - styles: Here, you'll find all the styling-related files, such as CSS, SCSS, or LESS. This directory includes style definitions and customizations for components and layouts.
  - types: The "types" directory stores TypeScript data types. It includes type definitions for the various data structures and interfaces used throughout your application.
  - views: The "views" directory contains components that represent the different pages or views displayed to users. These components are used within the "pages" directory and are responsible for rendering the content visible to users.

## Code Styleguide

1. Using useQuery hook for fetching data:

```js
interface User {
  id: number
  name: string
}

type IResponse = User[]

type IParams = {
  page?: number
  pageSize?: number
  options?: UseQueryOptions<IResponse, AxiosError>
}

function useUsers(params: IParams = {}) {
  const { page = 1, pageSize = 10, options } = params
  const queryKey = ['/users', page, pageSize]

  const query = useQuery<IResponse, AxiosError>(
    queryKey,
    () =>
      CallAPI.getDummyUsers({ page, pageSize }).then((res) => {
        return res.data
      }),
    { ...options },
  )

  return {
    ...query,
    data: query.data,
  }
}

export default useUsers
```

2. Using useMutation hook for POST, PUT, DELETE requests:

```js
type IParams = {
  onSuccess?: () => void,
}

function useLoginMutation(params: IParams = {}) {
  const { onSuccess } = params

  const mutation = useMutation(
    (fieldValues: { user: string, password: string }) => {
      return CallAPI.login(fieldValues)
    },
    {
      onSuccess() {
        onSuccess?.()
        // Action after login success here
      },
      onSettled() {
        // Action after login success or error here
      },
    },
  )

  return mutation
}

export default useLoginMutation
```

3. Using object parameter instead of multiple parameters in functions:

```js
// Bad
const fetchData = (param1, param2) => {
  // Fetch data using param1 and param2
}

// Good
const fetchData = ({ param1, param2 }) => {
  // Fetch data using param1 and param2
}
```

4. Code naming conventions:
   It's important to follow consistent naming conventions to make your code more readable. Here are some common conventions:

   - Use camelCase for variable and function names (e.g., myVariable, myFunction).
   - Use PascalCase for component names (e.g., MyComponent).
   - Use uppercase for constants (e.g., API_URL).
   - Choose descriptive names that convey the purpose or functionality of the code.
   - By adhering to these code rules, your code will be more organized, maintainable, and easier to understand.

5. Document your code:
   Consider adding comments or documentation to explain the purpose, functionality, and usage of your code. This helps other developers understand your codebase and enables easier collaboration and maintenance.
=======
# Nusantechfinal



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/SelloPradatara/nusantechfinal.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/SelloPradatara/nusantechfinal/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> b85f2dce15d9810d5b370a280492dd0e769a364d
